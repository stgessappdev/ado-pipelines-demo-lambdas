import AWS from 'aws-sdk';
import createHttpError from 'http-errors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function getGames(event, context) {
  let games;

  try {
    const result = await dynamodb.scan({
      TableName: process.env.GAMING_TABLE_NAME
    }).promise();

    games = result.Items;
  } catch(error) {
    console.error(error);
    throw new createHttpError.InternalServerError(error);
  }

  if(!games || games.length === 0){
    throw new createHttpError.NotFound('No Games found!');
  }

  return {
    statusCode: 200,
    // body: JSON.stringify({event, context}), // To debug uncomment
    body: JSON.stringify(games)
  };
}

export const handler = getGames;