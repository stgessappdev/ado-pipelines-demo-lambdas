import { v4 as uuid } from 'uuid';
import AWS from 'aws-sdk';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function createGame(event, context) {
  const { name, type, version } = JSON.parse(event.body);
  const now = new Date();

  const game = {
    id: uuid(),
    name,
    type,
    version,
    status: 'ACTIVE',
    createdAt: now.toISOString(),
  };

  await dynamodb.put({
    TableName: process.env.GAMING_TABLE_NAME,
    Item: game,
  }).promise();

  return {
    statusCode: 201,
    body: JSON.stringify(game),
  };
}

export const handler = createGame;